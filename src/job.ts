import { v4 as uuidv4 } from 'uuid'

// class to store details of a job posting
export class Job {
  _id: string;
  title: string;
  desc: string;
  location: string;

  constructor(_id: string, title: string, desc: string, location: string) {
    this._id = _id;
    this.title = title;
    this.desc = desc;
    this.location = location;
  }
}

// TODO: create a class to represent a job application

export function createJob(request): Job {
  // TODO: make backend call to post job and get its job id
  const id = uuidv4()
  return new Job(id, request.title, request.desc, request.location)
}

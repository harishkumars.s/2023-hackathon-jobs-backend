import { mongoUrl, database, jobsCollection, applicationCollection } from './constants'
const { MongoClient } = require('mongodb')
import { Job } from './job'

// create a new job
export async function addJob(job: Job): Promise<string> {
  const client = new MongoClient(mongoUrl);
  try {
    await client.connect()
    const db = client.db(database)
    const collectionObj = db.collection(jobsCollection)
    console.log('adding new job', job)
    const result = await collectionObj.insertOne(job)
    console.log('new job result', result)
    return result.insertedId
  }
  finally {
    client.close()
  }
}

// create a new job application
export async function applyJob(jobApplication): Promise<string> {
   const client = new MongoClient(mongoUrl)
  try {
    await client.connect()
    const db = client.db(database)
    const collectionObj = db.collection(applicationCollection)
    const result = await collectionObj.insertOne(jobApplication)
    return result.insertedId
  } finally {
    client.close()
  }
}

// retrieve all job postings
export async function getJobs(): Promise<Job[]> {
  const client = new MongoClient(mongoUrl);
  try {
    await client.connect()
    const db = client.db(database)
    const collectionObj = db.collection(jobsCollection)
    const cursor = await collectionObj.find()
    console.log('no of jobs', await cursor.count())
    const result = Array()
    await cursor.forEach((j) => {
      result.push(j)
    })
    return result
  }
  finally {
    client.close()
  }
}

// retrieve applications of a given job
export async function getApplicants(jobId: string): Promise<any[]> {
  const client = new MongoClient(mongoUrl);
  try {
    await client.connect()
    const db = client.db(database)
    const collectionObj = db.collection(applicationCollection)
    const cursor = await collectionObj.find({ jobId: { $eq: jobId }})
    const result = Array()
    await cursor.forEach((j) => {
      result.push(j)
    })
    return result
  }
  finally {
    client.close()
  }
}

// delete a job posting and associated applications
export async function deleteJob(jobId: string) {
  const client = new MongoClient(mongoUrl);
  try {
    await client.connect()
    const db = client.db(database)
    const applicationColObj = db.collection(applicationCollection)
    await applicationColObj.deleteMany({ jobId: jobId })
    const jobColObj = db.collection(jobsCollection)
    await jobColObj.deleteMany({ _id: jobId })
  }
  finally {
    client.close()
  }
}

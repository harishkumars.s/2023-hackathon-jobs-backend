import express from 'express'
import { setMaxIdleHTTPParsers } from 'http'
import { port, mongoUrl, } from './constants'
import { createJob } from './job'
import { addJob, getJobs, applyJob, getApplicants, deleteJob } from './mongodb'

const { MongoClient } = require('mongodb')

const app = express()

// configure express to parse JSON automatically
app.use(express.json())

// configure express to use 4 spaces when sending JSON as response
app.set('json spaces', 4)

const client = new MongoClient(mongoUrl);

client.connect().then(() => {
  console.log('mongo db connected')
})

app.post('/createJob', async (req, res) => {
  console.log('request:', req.body)
  const job = createJob(req.body)
  const newJob = await addJob(job)
  res.json({ id: job._id })
})

app.get('/jobs', async (req, res) => {
  console.log('request:', req.body)
  await sleep(500)
  const jobs = await getJobs()
  console.log('response', JSON.stringify(jobs))
  res.json(jobs)
})

app.post('/getApplicants', async (req, res) => {
  console.log('request:', req.body)
  await sleep(500)
  const applicants = await getApplicants(req.body.jobId)
  console.log('response', JSON.stringify(applicants))
  res.json(applicants)
})

// TODO: could be DELETE
app.post('/deleteJob', async (req, res) => {
  console.log('request:', req.body)
  await deleteJob(req.body.jobId)
  res.json('done')
})

app.post('/applyJob', async (req, res) => {
  console.log('request:', req.body)
  const appId = await applyJob(req.body)
  console.log('response', appId)
  res.json(appId)

})

app.listen(port, () => {
  return console.log(`Express is listening at http://localhost:${port}`);
});

function sleep(ms) {
  // TODO: sleep only in dev environments
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}


